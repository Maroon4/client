import React, { Component } from 'react';
// import './App.css';

class App extends Component {
    state = {users: []};

    componentDidMount() {
        fetch('/users')
            .then(res => res.json())
            .then(users => this.setState({ users }));
        console.log(JSON.stringify({users: []}, null, 3));
    }

    render() {
        return (
            <div>
                <h1>Users</h1>
                {this.state.users.map((user) => {
                    return (
                        <div key={user.id}>{user.username}</div>
                    );

                })}
            </div>
    );
    }
}

export default App;